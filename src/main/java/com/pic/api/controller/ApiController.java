package com.pic.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.pic.api.service.ApiService;

@RestController
public class ApiController {
	
	@Autowired
	ApiService service = new ApiService();

	
	public String getInfos() {
		return service.getJson();
	}
	
}
