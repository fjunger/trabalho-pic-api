package com.pic.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.pic.api.controller.ApiController;

@SpringBootApplication
public class MCapiMvcApplication {
	
	private static ApiController controller = new ApiController();

	public static void main(String[] args) {
		SpringApplication.run(MCapiMvcApplication.class, args);
		System.out.println(controller.getInfos());
		
	}

}
