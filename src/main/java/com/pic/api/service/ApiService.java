package com.pic.api.service;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ApiService {
	
//	StringBuilder strb = new StringBuilder();
//	
//	String moeda = "USD"; //EUR 
//	String dataInicial = "09-01-2021"; //dd-MM-aaaa
//	String dataFinal = "10-08-2021"; //dd-MM-aaaa
//	String top = "10000"; //qtd max de resultados
//	String format = "json";

//	strb.append("https://olinda.bcb.gov.br/olinda/servico/PTAX/versao/v1/odata/");
//	uri.append("CotacaoMoedaPeriodo(moeda=@moeda,dataInicial=@dataInicial,dataFinalCotacao=@dataFinalCotacao)");
//	uri.append("?@moeda='USD'");
//	uri.append("&@dataInicial='09-01-2021'");
//	uri.append("&@dataFinalCotacao='10-08-2021'");
//	uri.append("&$top=10000");
//	uri.append("&$format=json");
	
	String uri= "https://olinda.bcb.gov.br/olinda/servico/PTAX/versao/v1/odata/"
	+ "CotacaoMoedaPeriodo(moeda=@moeda,dataInicial=@dataInicial,dataFinalCotacao=@dataFinalCotacao)"
	+ "?@moeda='USD'"
	+ "&@dataInicial='09-01-2021'"
	+ "&@dataFinalCotacao='10-08-2021'"
	+ "&$top=10000"
	+ "&$format=json";
	
	
	public String getJson() {

		String[] json;
		RestTemplate restTemp = new RestTemplate();
		String result = restTemp.getForObject(uri,String.class);
		
		json=result.split(",");
		for(int i=0;i<json.length;i++) {
			System.out.println(json[i]);
		}
		
		return result;
		
	}
	
}
